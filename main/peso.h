/*
 * peso.h
 *
 *  Created on: Mar 31, 2021
 *      Author: na.gonzalo@gmail.com
 */

#ifndef MAIN_PESO_H_
#define MAIN_PESO_H_

#include <esp_types.h>

typedef struct
{
	int32_t bruto;      // Idem a la anterior pero con el incremento
	int32_t tara;       //
	int32_t neto;       //
}peso_t;

/*
 * calcular_peso(int32_t data_in)
 * Funci�n que toma los valores en el formato entregado por el ADC y con eso
 * calcula el peso seg�n la recta de calibraci�n.
 *
 * @param data in Valor de 32 bit con signo que representa la lectura del
 * conversor ADS1231 filtrada
 * */
void peso_init(void);

void tomaTara(void);
void tomaDeCero(void);

#endif /* MAIN_PESO_H_ */
