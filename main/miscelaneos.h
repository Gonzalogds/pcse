/*
 * miscelaneos.h
 *
 *  Created on: Apr 2, 2021
 *      Author: na.gonzalo@gmail.com
 */

#ifndef MAIN_MISCELANEOS_H_
#define MAIN_MISCELANEOS_H_

#include <esp_types.h>

/**
 * Toma como argumento un float y devuelve el
 * entero con signo m�s cercano
 *
 * @param   N�mero a redondear: float
 * @return  Resultado redondeado: int32_t
 */
int32_t redondeo (float numero);


int32_t redondeoHisteresis (float nuevo, float anterior);

/**
 * Devuelve el valor absoluto de un n�mero entero
 *
 * @param numero n�mero con signo
 * @return n�mero sin signo
 */
uint32_t absEntero (int32_t numero);


/**
 * Devuelve el valor absoluto de un n�mero float
 *
 * @param numero n�mero con signo
 * @return n�mero sin signo
 */
float absFloat (float numero);
#endif /* MAIN_MISCELANEOS_H_ */
