/*
 * miscelaneos.c
 *
 *  Created on: Apr 2, 2021
 *      Author: na.gonzalo@gmail.com
 */


#include "miscelaneos.h"

/**
 * Toma como argumento un float y devuelve el
 * entero con signo m�s cercano
 *
 * @param   N�mero a redondear: float
 * @return  Resultado redondeado: int32_t
 */
int32_t redondeo (float numero)
{
	if(numero > 0.0)
	{
		numero += 0.5;
	}
	else
	{
		numero -= 0.5;
	}

	return (int32_t)numero;
}


int32_t redondeoHisteresis (float nuevo, float anterior)
{
	float delta  = 0;

	delta =  absFloat(nuevo - anterior);
	if(delta < 0.1) delta = anterior;
	else delta = nuevo;

	return redondeo(delta);
}

/**
 * Devuelve el valor absoluto de un n�mero entero
 *
 * @param numero n�mero con signo
 * @return n�mero sin signo
 */
uint32_t absEntero (int32_t numero)
{
	if(numero<0)
	{
		numero=-numero;
	}

	return (uint32_t)numero;
}

/**
 * Devuelve el valor absoluto de un n�mero float
 *
 * @param numero n�mero con signo
 * @return n�mero sin signo
 */
float absFloat (float numero)
{
	if(numero<0)
	{
		numero=(-numero);
	}
	return numero;
}
