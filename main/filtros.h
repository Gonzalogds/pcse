/*
 * filtros.h
 *
 *  Created on: Mar 29, 2021
 *      Author: na.gonzalo@gmail.com
 */

#ifndef MAIN_FILTROS_H_
#define MAIN_FILTROS_H_

#include <esp_types.h>

int32_t filtroPrincipal(int32_t adc_in);

#endif /* MAIN_FILTROS_H_ */
