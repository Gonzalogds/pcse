/*
 * PCA9555A.h
 *
 *  Created on: Mar 6, 2021
 *      Author: na.gonzalo@gmail.com
 */

#ifndef MAIN_PCA9555A_H_
#define MAIN_PCA9555A_H_
#include "esp_err.h"

#define PCA9555_I2C_ADDR0 0x20
#define CONF_PORT_IN	0xFF
#define CONF_PORT_OUT	0x00


esp_err_t  pca9555a_config_port(uint8_t address, uint8_t port, uint8_t conf);

esp_err_t  pca9555a_write_port(uint8_t address, uint8_t port, uint8_t data_wr);

esp_err_t  pca9555a_read_port(uint8_t address, uint8_t port, uint8_t* data_rd);

#endif /* MAIN_PCA9555A_H_ */
