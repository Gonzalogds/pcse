/*
 * peso.c
 *
 *  Created on: Mar 31, 2021
 *      Author: na.gonzalo@gmail.com
 */


/******************************************************************************
 * Includes
 *****************************************************************************/
#include "peso.h"
#include "ADS1231.h"
#include "miscelaneos.h"
#include "filtros.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"


/******************************************************************************
 * Defines and typedefs
 *****************************************************************************/
#define TIEMPO_CERO_AUTOM       (10000)   // Per�odo de toma de cero autom�tico
#define LIMITE_MCAP				(-20)
#define LIMITE_SCAP_MET_LEGAL   (  8)
#define CTE_KG_LB 				2.20462

/******************************************************************************
 * Local variables
 *****************************************************************************/
typedef struct
{
	// Rango
	//   -20mv -------------- 0mv ----------- 20mv
	// 0x800000h           0x000000h        0x7FFFFFh
	// -8388608					0			8,388,607
	int32_t  cero; //diferencia entre cero de calib y cero de uso
	int32_t  ceroOffset;//delta cuentas para la toma de cero (4% capMax)
	int32_t  bruto;
	int32_t  neto;
}cuentas_t;

typedef struct
{
	float   fdivisiones;
	float   fbruto;  //NO se afecta por el punto decimal
	float   ftara;	 //NO se afecta por el punto decimal
	float   fneto;	 //NO afecta por el punto decimal

	float   fdbruto; //SI se afecta por el punto decimal
	float   fdtara;	 //SI se afecta por el punto decimal
	float   fdneto;	 //SI se afecta por el punto decimal
}peso_float_t;

typedef struct
{
	int32_t incremento;
	uint32_t puntoDecimal;
	int32_t capMax;
	int32_t cuentas[5];//el numero 0 es el cero de calib
	float    span[4];
	int32_t param[12];// futuras ampliaciones
}
ajuste_t;

//static uint32_t tiempo10seg=0;

static cuentas_t cuentas;
static peso_t peso;
static peso_float_t peso_float;

static ajuste_t ajuste =
{
	.incremento 	= 1,
	.puntoDecimal 	= 0,
	.capMax			= 10000,
	.cuentas 		= {0,0,0,0,0},
	.span			= {0.00155438634,0,0,0},
};


QueueHandle_t peso_queue = 0;

// Create a queue capable of containing 10 uint32_t values.

/******************************************************************************
 * Local Prototypes
 *****************************************************************************/
static void ceroAutomatico(void);
/*****************************************************************************
 * Local Functions
 *****************************************************************************/
void ceroAutomatico(void)
{
	/*if((estado.movimiento==0)&&								 //No hay movimiento
	   (funcion->funcion1!=FUNC_TANQUES)&&					 //No estoy estado tanque
	   (estabilidad->ceroAutomatico) &&					     //Cero autom�tico habilitado
	   (estado.neto == 0) &&
	   (timer32DifTiempo(tiempo10seg)>TIEMPO_CERO_AUTOM) )	//Pasaron 10 segundos desde el CA anterior
	   {
		tiempo10seg=timer32TiempoGlobal();

		if( (absEntero(peso.divisiones)*10) < estabilidad->ceroAutomatico )
		{
			if( (cuentas.filtro < (ajuste.cuentas[0] + cuentas.ceroOffset)) &&
				(cuentas.filtro > (ajuste.cuentas[0] - cuentas.ceroOffset)))
			{
				cuentas.cero   		= 	cuentas.filtro;
				peso.divisiones		=	0;			//Recalculo divisiones
				cuentas.bruto		=	0;			//Recalculo bruto
				peso.fdivisiones	=	0;			//Ver de guardar este valor
			}
		}
	}*/
}

void calcular_peso(int32_t data_in)
{
	static float fdivisionesAnterior = 0;
	int32_t divisiones;
	uint32_t divisor = 0;
	uint8_t incremento2 = 0;
	uint8_t flags = 0;

	uint8_t n = 1,i = 0;
	float P  = 0;
	int32_t delta;

	data_in = filtroPrincipal(data_in);

	//TODO �debemos hacer comprobaci�n de estado de error?
	if(1) //estado.errorInicial==ERROR_NO && !estado.calibracion)
	{
		tomaDeCero();


		delta = cuentas.cero - ajuste.cuentas[0];
		cuentas.bruto =  (data_in - delta);



		if(cuentas.bruto < ajuste.cuentas[1] || ajuste.cuentas[1] == 0)
			n = 1;
		else if(cuentas.bruto < ajuste.cuentas[2] || ajuste.cuentas[2] == 0)
			n = 2;
		else if(cuentas.bruto < ajuste.cuentas[3] || ajuste.cuentas[3] == 0)
			n = 3;
		else if(cuentas.bruto < ajuste.cuentas[4] || ajuste.cuentas[4] == 0)
			n = 4;

		for (i = 1 ; i < n ; i++)
			P += ((float)(ajuste.cuentas[i]-ajuste.cuentas[i-1])) * ajuste.span[i-1];

		peso_float.fdivisiones = ((float)cuentas.bruto - (float)ajuste.cuentas[n-1]) * ajuste.span[n-1] + P;

		divisiones  = redondeoHisteresis(peso_float.fdivisiones,fdivisionesAnterior); //
		fdivisionesAnterior = peso_float.fdivisiones;

		ceroAutomatico();

		//TODO: registrar error de menos cap
		/*if( estado.x10 == 0 && errorDevolver()!=A_0FR )
		{
			if( peso.divisiones < LIMITE_MCAP &&
			   (habilitaMenosCap & (1 << funcion->funcion1) ) && regional->habilitarFunciones == 0)
			{
				errorMostrar(E_MCAP);
				estado.neto = 0;       //con error de bruto positivo forzamos el estado de bruto
			}
			if(peso.divisiones > (int32_t)(ajuste->capMax/ajuste->incremento + LIMITE_SCAP_MET_LEGAL))
			{
				errorMostrar(E_SCAP);
			}
		}*/

		peso_float.fbruto = peso_float.fdivisiones * ajuste.incremento;
		peso.bruto  = divisiones  * ajuste.incremento;

		tomaTara();

		/*if(pedido.volverABruto == 1)
		{
			pedido.volverABruto = 0;
			estado.neto = 0;
			peso.maximo = 0;
		}
		else if(pedido.pasarANeto == 1)
		{
			pedido.pasarANeto = 0;
			estado.neto = 1;
		}*/

		//TODO: resolver toma de tara
		/*if( 0 estado.neto == 1)
			peso.tara = peso.taraTomada;
		else
			peso.tara = 0;*/

		peso_float.ftara = (float) peso.tara;

		peso.neto  = peso.bruto  - peso.tara;
		peso_float.fneto = peso_float.fbruto - peso.tara; //se toma tara entera tipo O.I.M.L.

		//TODO: resolver flag de cero
		/*if(!estado.neto && peso.neto==0)
			estado.cero = 1;
		else
			estado.cero = 0;*/

		/*if(funcion->unidadPrincipal == KGR)
		{
			incremento2 = ajuste->incremento;
			pesoUnidad.bruto = incremento2 * redondeo(peso.fbruto * CTE_KG_LB / incremento2);
			pesoUnidad.tara  = incremento2 * redondeo((float)peso.tara  * CTE_KG_LB / incremento2);
			pesoUnidad.neto = pesoUnidad.bruto - pesoUnidad.tara;
		}
		else pesoUnidad.bruto = pesoUnidad.tara  = pesoUnidad.neto = 0;*/

		//TODO: resolver flags de estado metrologico
		/*calculo de flag
		flags |=  (estado.neto << 3) | (estado.movimiento << 2)
			  |   (estado.cero << 1) | (estado.taraManual << 5);
		if((uint8_t) errorDevolver() < E_MAXIMO)
			flags |= (1<<7);
		if(peso.bruto < 0)
			flags |= (1<<4);
		if(estado.neto)
			flags |= (peso.neto  < 0)? (1 << 0) : (0 << 0) ;
		else
			flags |= (peso.bruto < 0)? (1 << 0) : (0 << 0) ;

		flagsEstado = (uint32_t) flags;*/

		//TODO: resolver calculo de peso decimal para automatismos
		/*switch (funcion->funcion1)
		{
		case FUNC_CONTADOR:
		case FUNC_PORCENTUAL:
			piezas.neto =  redondeo(peso.fneto * piezas.patron);
			break;
		case FUNC_RETMAXIMA:
			if(peso.neto > peso.maximo) peso.maximo = peso.neto;
			break;
		case FUNC_ABOK:
		case FUNC_ENVASADO:
		case FUNC_ENVASADO_NET:
		case FUNC_DOSI_MANUAL:
		case FUNC_DOSI_AUTO:
		case FUNC_DESPACHO:
		case FUNC_SPC:
		case FUNC_AUTOMATA:
			divisor = powerDiez(ajuste->puntoDecimal);
			peso.fdbruto = peso.fbruto / divisor;
			peso.fdneto  = peso.fneto  / divisor;
			peso.fdtara  = peso.ftara  / divisor;
			break;
		default:
			piezas.neto = 0;
		}*/

		xQueueSend( peso_queue, &peso,  0 );
	}
}

/******************************************************************************
 * Public Functions
 *****************************************************************************/
void peso_init(void)
{
	//TODO aqui podria hacer la inicializaci'on de los parametros de ajuste

	ads1231_init(calcular_peso);

	peso_queue = xQueueCreate( 2, sizeof( peso_t ) );
	if( peso_queue == 0 )
	{
	    //todo Queue was not created and must not be used.
	}
}
void tomaDeCero(void)
{
	/*if((pedido.tomaDeCero==1)&&(estado.movimiento==0))
	{
		if( (cuentas.filtro > (ajuste.cuentas[0] + cuentas.ceroOffset)) ||
			(cuentas.filtro < (ajuste.cuentas[0] - cuentas.ceroOffset)))
		{
			errorMostrar(A_0FR);
		}
		else
		{
			cuentas.cero = cuentas.filtro;
			if(pedido.guardarCero)
				memoriaEscribirBloque((uint8_t*) &cuentas.cero,OFFSET_CERO_TANQUE,sizeof(cuentas.cero));
		}
		pedido.tomaDeCero=0;
		pedido.guardarCero = 0;
	}*/
}

void tomaTara(void)
{
	if( 0/*(pedido.tomaDeTara==1) && (estado.movimiento==0)*/ )
	{
		//pedido.tomaDeTara = 0;

		if( peso.bruto >= 0 )
		{
			peso.tara = peso.bruto;
			//peso.maximo = 0;
			//estado.neto = 1;
		}
	}
}
