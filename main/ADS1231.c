/*
 * ADS1231.c
 *
 *  Created on: Feb 9, 2021
 *      Author: Sanchez Gonzalo - na.gonzalo@gmail.com
 */

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "sdkconfig.h"

#include "ADS1231.h"
#include "filtros.h"

#define ADS1231_TASK_PRIORITY	2
#define ADS1231_TASK_CORE	1//tskNO_AFFINITY
#define GPIO_ADS_INT_DAT    2

#define PIN_NUM_MISO GPIO_ADS_INT_DAT
#define PIN_NUM_CLK  0

#define BITS_TO_READ 24
#define ADC_TIME_OUT 150

#define ADS_SPI_CLK	100000 //100kHz

/*-----------------------------------[Variables locales]-------------------------*/
static TaskHandle_t ADS1231TaskToNotify;

static spi_device_handle_t spi_handle;

static adc_process_t callback_function = NULL;

/*-----------------------------------[Prototipos locales]-------------------------*/
void spi_init(void);
int32_t read_ads1231(void);
int32_t int24_to_Int32(uint8_t* byteArray);

/*-----------------------------------[Funciones locales]--------------------------*/

int32_t int24_to_Int32(uint8_t* byteArray)
{
	int result = (
				 ((0xFF & byteArray[0]) << 16) |
				 ((0xFF & byteArray[1]) << 8) |
				  (0xFF & byteArray[2]) );

	 if (result & 0x00800000)
	 {
	   result |= 0xFF000000;
	 }
	 else
	 {
	   result &= 0x00FFFFFF;
	 }
	return result;
}

static void IRAM_ATTR gpio_isr_handler(void* arg)
{
	vTaskNotifyGiveFromISR(  ADS1231TaskToNotify, (BaseType_t *) NULL );
}

void spi_init(void)
{
    esp_err_t ret;

    spi_bus_config_t buscfg =
    {
        .miso_io_num = PIN_NUM_MISO,
        .mosi_io_num = -1,
        .sclk_io_num = PIN_NUM_CLK,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .max_transfer_sz = 5,
		.flags = SPICOMMON_BUSFLAG_MASTER
    };

    //Initialize the SPI bus
    ret = spi_bus_initialize(HSPI_HOST, &buscfg, 0);
    ESP_ERROR_CHECK(ret);

    spi_device_interface_config_t devcfg =
    {
        .clock_speed_hz = ADS_SPI_CLK,           //Clock out at 1 MHz
        .mode = 0,                               //SPI clock polarity and phase mode
        .spics_io_num = -1,  		             //CS pin
        .queue_size = 1,                         //We want to be able to queue just 1 transactions at a time
        .pre_cb = NULL,
    };

    //Attach the ADC to the SPI bus
    ret = spi_bus_add_device(HSPI_HOST, &devcfg, &spi_handle);
    ESP_ERROR_CHECK(ret);
}

int32_t read_ads1231(void)
{
	int32_t read_value;

	gpio_set_intr_type(GPIO_ADS_INT_DAT, GPIO_INTR_DISABLE);

	spi_transaction_t trans =
	{
		.flags = SPI_TRANS_USE_RXDATA,
		.length = BITS_TO_READ,
		.rxlength = BITS_TO_READ,

	};

	esp_err_t err = spi_device_polling_transmit(spi_handle, &trans);
	if (err!= ESP_OK)
		printf("error de lectura\n");

	gpio_set_intr_type(GPIO_ADS_INT_DAT, GPIO_INTR_NEGEDGE);
#if BITS_TO_READ == 25
	trans.rx_buffer >>= 1;
#endif
	read_value = int24_to_Int32(trans.rx_data);

	return read_value;
}

void ads1231_task(void* args)
{
	int32_t cuentas = 0;

    printf("ADS1231 task started");
    //install gpio isr service
    gpio_install_isr_service(ESP_INTR_FLAG_LEVEL1);
    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(GPIO_ADS_INT_DAT, gpio_isr_handler, (void*) NULL); //ver ultimo argumento

    gpio_config_t io_conf;

    //interrupt of rising edge
    io_conf.intr_type = GPIO_INTR_NEGEDGE;
    //bit mask of the pins, use GPIO4/5 here
    io_conf.pin_bit_mask = (1ULL << GPIO_ADS_INT_DAT);
    //set as input mode
    io_conf.mode = GPIO_MODE_INPUT;
    //disable pull-down mode
    io_conf.pull_down_en = 0;
    //disable pull-up mode
    io_conf.pull_up_en = 1;
    gpio_config(&io_conf);

    spi_init();

    for(;;)
    {

    	if(ulTaskNotifyTake( pdTRUE, ADC_TIME_OUT / portTICK_PERIOD_MS))
    	{
            cuentas = read_ads1231();
            if(NULL != callback_function)
            	callback_function(cuentas);
        }
    	else
    	{
    		//TODO: aca deberiamos disparar de alguna forma el error de conversor
    	}
    }
}

/*----------------------------[Funciones p�blicas]------------------------------------*/
void ads1231_init(adc_process_t funcion)
{
    callback_function = funcion;

    portBASE_TYPE res = xTaskCreatePinnedToCore(&ads1231_task,
    											"ads1231",
                                                2048,
												NULL,
												ADS1231_TASK_PRIORITY,
												&ADS1231TaskToNotify,
												ADS1231_TASK_CORE);
    //assert(res == pdTRUE);

}

void set_callback_function(adc_process_t function)
{
//TODO implementar alguna seguridad de c�digo cr�tico aqui
	callback_function = function;
}
