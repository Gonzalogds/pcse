/*
 * teclado.c
 *
 *  Created on: Mar 11, 2021
 *      Author: na.gonzalo@gmail.com
 *
 * Si bien el fin principal de este archivo es manejar
 * el teclado frontal del equipo, tambien maneja las se�ales
 * del cargador de bateia. EN esta primera versi�n el segundo
 * puerto no se utiliza.
 * Se mapea el GPIO expander como un �nico puerto de 16 bits
 */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "PCA9555A.h"
#include "teclado.h"
#include "I2C.h"

#define TECLADO_TASK_PRIORITY	2
#define TECLADO_TASK_CORE		tskNO_AFFINITY

#define DIR_EXP_INTER PCA9555_I2C_ADDR0 //direccion del expansor interno
#define DELAY_REBOTE  50
#define CANT_IO		16

#define IN 1
#define OUT 0
#define Pin(pin , conf) (conf << pin)
//Macros que permiten configurar los pines como entrada/salida individualmente
#define CONF_PORT0 (Pin(7,IN) | Pin(6,IN) | Pin(5,IN)| Pin(4,IN)| Pin(3,IN)| Pin(2,IN)| Pin(1,IN)| Pin(0,IN))
#define CONF_PORT1 (Pin(7,OUT) | Pin(7,OUT) | Pin(5,OUT)| Pin(4,OUT)| Pin(3,OUT)| Pin(2,OUT)| Pin(1,OUT)| Pin(0,OUT))

#define check_bit(bit , reg) (reg & (1<<bit))

/*-----------------------------------[Variables locales]-------------------------*/
typedef struct
{
	union
	{
		uint16_t inputs;
		uint8_t ports_in[2];
	};
	union
	{
		uint16_t outputs;
		uint8_t ports_out[2];
	};
}IO_t;

static IO_t expander_io;

typedef struct
{
    uint8_t pin;
    est_tec estado;
    evento_tec evento;
}tecla_t;

tecla_t teclas[] =
{
		{
			.pin = 1,
			.estado = up,
			.evento = key_undefined,
		},

		{
			.pin = 2,
			.estado = up,
			.evento = key_undefined,
		},

		{
			.pin = 3,
			.estado = up,
			.evento = key_undefined,
		},
		{
			.pin = 4,
			.estado = up,
			.evento = key_undefined,
		}
};

SemaphoreHandle_t key_semaphore = NULL;
StaticSemaphore_t key_mutex_buffer;

/*---------------------------[Local functions]--------------------------------------*/
esp_err_t teclado_config(void)
{
	esp_err_t ret;
	ret = pca9555a_config_port(DIR_EXP_INTER,0, CONF_PORT0);
	if(ret)
		return ret;
	ret = pca9555a_config_port(DIR_EXP_INTER,1, CONF_PORT1);

	return ret;
}

esp_err_t read_imputs()
{
	esp_err_t ret;
	ret = pca9555a_read_port(DIR_EXP_INTER,0, &expander_io.ports_in[0]);
	if(ret)
		return ret;
	ret = pca9555a_read_port(DIR_EXP_INTER,1, &expander_io.ports_in[1]);

	expander_io.ports_in[0] &= CONF_PORT0;
	expander_io.ports_in[1] &= CONF_PORT1;
	return ret;
}

void lee_tecla(tecla_t* tecla)
{
    if((void*) tecla == NULL)
        return;
    if(tecla->pin > CANT_IO)
    {
    	tecla->evento = key_undefined;
    	return;
    }

    switch (tecla->estado)
    {
    case up:
		if(check_bit(tecla->pin , expander_io.inputs))
		{
			tecla->estado = up;
		}
		else
		{
			tecla->estado = falling;
			tecla->evento = key_undefined;
		}
		break;
    case down:
		if(check_bit(tecla->pin , expander_io.inputs))
		{
			tecla->estado = rising;
			tecla->evento = key_undefined;
		}
		else
		{
			tecla->estado = down;
		}
		break;
    case falling:
	   if(check_bit(tecla->pin , expander_io.inputs))
	   {
		   tecla->estado = down;
		   tecla->evento = key_pressed;
	   }
	   else
	   {
		   tecla->estado = up;
	   }
       break;
    case rising:
	   if(check_bit(tecla->pin , expander_io.inputs))
	   {
		   tecla->estado = up;
		   tecla->evento = key_released;
	   }
	   else
	   {
		   tecla->estado = down;
	   }
    }
}

void leer_teclado()
{
	uint8_t i;

	for (i = 0 ; i < (sizeof(teclas)/sizeof(tecla_t)) ; i++)
	{
		lee_tecla( teclas+i );
	}

}

void teclado_task(void* args)
{
    key_semaphore = xSemaphoreCreateMutexStatic( &key_mutex_buffer );

    configASSERT( key_semaphore );

	if(teclado_config() != ESP_OK)
	{
		//vTaskDelete(NULL);
	}

	while(1)
	{
		read_imputs();

		xSemaphoreTake( key_semaphore, ( TickType_t ) portMAX_DELAY );
		leer_teclado();
		xSemaphoreGive(key_semaphore);

		vTaskDelay(DELAY_REBOTE/portTICK_PERIOD_MS );
	}
}
/*-----------------------------[Public functions]-------------------------------------*/

void teclado_init(void)
{
	internal_i2c_init();

    portBASE_TYPE res = xTaskCreatePinnedToCore(&teclado_task,
    											"teclado",
                                                2048,
												NULL,
												TECLADO_TASK_PRIORITY,
												NULL,
												TECLADO_TASK_CORE);
    assert(res == pdTRUE);
}

uint8_t get_key_event(uint8_t key_number)
{
	evento_tec event;

	xSemaphoreTake( key_semaphore, ( TickType_t ) portMAX_DELAY );
	event = teclas[key_number].evento;
	teclas[key_number].evento = key_undefined;
	xSemaphoreGive(key_semaphore);

	return event;
}

uint8_t get_key_state(uint8_t key_number)
{
	est_tec state;

	xSemaphoreTake( key_semaphore, ( TickType_t ) portMAX_DELAY );
	state = teclas[key_number].estado;
	xSemaphoreGive(key_semaphore);

	return state;
}

uint8_t get_input_state(uint8_t input)
{
	uint8_t in;
	if(input > CANT_IO)
		return 0;

	xSemaphoreTake( key_semaphore, ( TickType_t ) portMAX_DELAY );
	in = (expander_io.inputs & (1 << input))?1:0;
	xSemaphoreGive(key_semaphore);

	return in;
}






