/*
 * I2C.c
 *
 *  Created on: Mar 6, 2021
 *      Author: na.gonzalo@gmail.com
 */

#include "I2C.h"
#include "driver/i2c.h"
#include "freertos/semphr.h"


/**
 * CUIDADO: Los drivers del I2C no son thread safe. Cuidado con usar el
 * mismo puerto desde dos hilos distintos.
 */



#define I2C_MASTER_TX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define I2C_MASTER_TX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define WRITE_BIT I2C_MASTER_WRITE  /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ    /*!< I2C master read */

#define I2C_TIME_OUT 5 //milisegundos que el driver espera una respuesta del esclavo
#define I2C_TIME_WAIT	20//tiempo en milisegundos que esperar� una tarea si quiere llamar al recurso y este est� ocupado

#define I2C_TICKS_OUT (I2C_TIME_OUT/configTICK_RATE_HZ)
#define I2C_TICKS_WAIT (I2C_TIME_WAIT/configTICK_RATE_HZ)

/*---------------------------[local variables]-----------------------*/
 SemaphoreHandle_t i2c_semaphore = NULL;
 StaticSemaphore_t i2c_mutex_buffer;

//TODO: ver de hacer una comprobaci�n de drivers instalado

esp_err_t i2c_init_master(uint8_t port, uint32_t clk, int scl_io, int sda_io)
{

	i2c_config_t conf =
	{
	    .mode = I2C_MODE_MASTER,
	    .sda_io_num = sda_io,
	    .sda_pullup_en = GPIO_PULLUP_ENABLE,
	    .scl_io_num = scl_io,
	    .scl_pullup_en = GPIO_PULLUP_ENABLE,
	    .master.clk_speed = clk,  // select frequency specific to your project
	    // .clk_flags = 0,          /*!< Optional, you can use I2C_SCLK_SRC_FLAG_* flags to choose i2c source clock here. */
	};

	esp_err_t ret = i2c_param_config((i2c_port_t)port, &conf);
    if (ret != ESP_OK) {
        return ret;
    }

    i2c_semaphore = xSemaphoreCreateMutexStatic( &i2c_mutex_buffer );

    configASSERT( i2c_semaphore );

	return i2c_driver_install(port, I2C_MODE_MASTER, I2C_MASTER_TX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0);

}

esp_err_t write_i2c(uint8_t i2c_port, uint8_t address, uint8_t* data_wr, uint32_t size,  uint8_t ack)
{
	esp_err_t ret;

    if( xSemaphoreTake( i2c_semaphore, ( TickType_t ) I2C_TICKS_WAIT ) == pdTRUE )
    {
        i2c_cmd_handle_t cmd = i2c_cmd_link_create();
        i2c_master_start(cmd);
        i2c_master_write_byte(cmd, (address << 1) | WRITE_BIT, ack);
        i2c_master_write(cmd, data_wr, size, ack);
        i2c_master_stop(cmd);
        ret = i2c_master_cmd_begin(i2c_port, cmd, I2C_TICKS_OUT);
        i2c_cmd_link_delete(cmd);

        xSemaphoreGive( i2c_semaphore );
    }
    else
    {
    	ret = ESP_FAIL;
    }
    //assert(ret == ESP_ERR_INVALID_STATE);
    return ret;
}

esp_err_t read_i2c(uint8_t i2c_port, uint8_t address, uint8_t* data_rd, uint32_t size)
{
	esp_err_t ret;

    if (size == 0) {
        return ESP_OK;
    }

    if( xSemaphoreTake( i2c_semaphore, ( TickType_t ) I2C_TICKS_WAIT ) == pdTRUE )
    {
        i2c_cmd_handle_t cmd = i2c_cmd_link_create();
        i2c_master_start(cmd);
        i2c_master_write_byte(cmd, (address << 1) | READ_BIT, ACK_CHECK_EN);
        if (size > 1)
        {
            i2c_master_read(cmd, data_rd, size - 1, ACK_VAL);
        }
        i2c_master_read_byte(cmd, data_rd + size - 1, ACK_VAL);
        i2c_master_stop(cmd);
        ret = i2c_master_cmd_begin(i2c_port, cmd, I2C_TICKS_OUT);
        i2c_cmd_link_delete(cmd);

        xSemaphoreGive( i2c_semaphore );
    }
    else
    {
    	ret = ESP_FAIL;
    }
    //assert(ret == ESP_ERR_INVALID_STATE);
    return ret;
}
