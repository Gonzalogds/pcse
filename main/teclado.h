/*
 * teclado.h
 *
 *  Created on: Mar 27, 2021
 *      Author: na.gonzalo@gmail.com
 */

#ifndef MAIN_TECLADO_H_
#define MAIN_TECLADO_H_

typedef enum
{
    up = 0,
    down,
	falling,
    rising
}est_tec;

typedef enum
{
    key_undefined = 0,
    key_released,
    key_pressed,
	key_holded
}evento_tec;

void teclado_init(void);

uint8_t get_key_event(uint8_t key_number);

uint8_t get_key_state(uint8_t key_number);

uint8_t get_input_state(uint8_t input);


#endif /* MAIN_TECLADO_H_ */
