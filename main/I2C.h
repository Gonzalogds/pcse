/*
 * I2C.h
 *
 *  Created on: Mar 6, 2021
 *      Author: na.gonzalo@gmail.com
 */
#include "esp_err.h"

#ifndef MAIN_I2C_H_
#define MAIN_I2C_H_

#define ACK_CHECK_EN 0x1            /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0           /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                 /*!< I2C ack value */
#define NACK_VAL 0x1                /*!< I2C nack value */

#define I2C_INT_SDA_IO	13
#define I2C_INT_SCL_IO	14
#define I2C_INT_SCL_HZ	100000
#define I2C_INT_PORT	1 // puerto i2c interno, teclado y display

#define I2C_EXT_PORT	0 // puerto i2c externo, placa de corte y otros
#define I2C_EXT_SCL_HZ  100000
#define I2C_EXT_SDA_IO	0
#define I2C_EXT_SCL_IO	2


esp_err_t i2c_init_master(uint8_t port, uint32_t clk, int scl_io, int sda_io);

#define internal_i2c_init() i2c_init_master(I2C_INT_PORT, I2C_INT_SCL_HZ, I2C_INT_SCL_IO, I2C_INT_SDA_IO)

#define external_i2c_init() i2c_init_master(I2C_EXT_PORT, I2C_EXT_SCL_HZ, I2C_EXT_SCL_IO, I2C_EXT_SDA_IO)

esp_err_t write_i2c(uint8_t i2c_port, uint8_t address, uint8_t* data_wr, uint32_t size,  uint8_t ack);
esp_err_t read_i2c(uint8_t i2c_port, uint8_t address, uint8_t* data_rd, uint32_t size);

#define write_i2c_int(address, data_wr, size, ack) write_i2c(I2C_INT_PORT,  address, data_wr,  size,  ack)
#define read_i2c_int(address, data_rd, size) read_i2c(I2C_INT_PORT, address, data_rd, size)
#endif /* MAIN_I2C_H_ */
