#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "sdkconfig.h"

#include "ADS1231.h"
#include "teclado.h"
#include "peso.h"


#include "PCA9555A.h"
#include "I2C.h"
void init_perifericos(void);

/* Can use project configuration menu (idf.py menuconfig) to choose the GPIO to blink,
   or you can edit the following line and set a number here.
*/
#define BLINK_GPIO 4
#define PERIODO 1000

extern QueueHandle_t peso_queue;

void app_main(void)
{
	uint32_t i;
	peso_t peso = {0,0,0};

	init_perifericos();

	if(NULL == peso_queue)
	{
		//todo: hacer manejo de errores
	}

    while(1)
    {
        if(xQueueReceive( peso_queue, &peso,  PERIODO / portTICK_PERIOD_MS ))
        	printf("peso %08u \r\n",peso.bruto);
        else
        	printf("Error comunicacion \r\n");

        for( i = 0 ; i < 5 ; i++)
        {
        	if(key_pressed == get_key_event((uint8_t) i))
        	{
        		printf("se presiono tecla %u \r\n",i);
        	}
        }

    }
}

/*Funcion para inicializar los perifericos y configurar los GPIO del modulo
 * NOTA: Ver si es conveniente ejecutarla */
void init_perifericos(void)
{

	peso_init();
    teclado_init();

}
