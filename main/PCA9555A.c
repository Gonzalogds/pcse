/*
 * PCA9555A.c
 *
 *  Created on: Mar 6, 2021
 *      Author: na.gonzalo@gmail.com
 */

#include "PCA9555A.h"
#include "I2C.h"
#include "esp_err.h"

#define DIR0_PCA	0x20
#define DIR_TECLADO DIR0_PCA


//registers
#define PCA9555_INPUT_0  (0)
#define PCA9555_INPUT_1  (1)
#define PCA9555_OUTPUT_0 (2)
#define PCA9555_OUTPUT_1 (3)
#define PCA9555_POLINV_0 (4)
#define PCA9555_POLINV_1 (5)
#define PCA9555_CONFIG_0 (6)
#define PCA9555_CONFIG_1 (7)

#define PCA9555_PIN_IN	1
#define PCA9555_PIN_OUT	0



esp_err_t  pca9555a_config_port(uint8_t address, uint8_t port, uint8_t conf)
{
	uint8_t buff[2];
	if(port > 1)
		return ESP_FAIL;
	buff[0] = PCA9555_CONFIG_0 + port;
	buff[1] = conf;
	return write_i2c_int(address, buff, 2, ACK_CHECK_EN);
}

esp_err_t  pca9555a_write_port(uint8_t address, uint8_t port, uint8_t data_wr)
{
	uint8_t buff[2];
	if(port > 1)
		return ESP_FAIL;
	buff[0] = PCA9555_OUTPUT_0 + port;
	buff[1] = data_wr;
	return write_i2c_int(address, buff, 2, ACK_CHECK_EN);
}

esp_err_t  pca9555a_read_port(uint8_t address, uint8_t port,uint8_t* data_rd)
{

	if(port > 1)
		return ESP_FAIL;

	port = PCA9555_INPUT_0 + port;
	write_i2c_int(address, &port, 1, ACK_CHECK_DIS);

	return read_i2c_int(address, data_rd, 1);
}
