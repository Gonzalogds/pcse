/*
 * filtros.c
 *
 *  Created on: Mar 28, 2021
 *      Author: na.gonzalo@gmail.com
 */

#include "filtros.h"

/******************************************************************************
 * Defines and typedefs
 *****************************************************************************/
#define WINDOWS_LENGHT		(50)
//TODO: nivelPromedios se deber� reemplazar por el valor de configuraci
#define nivelPromedios WINDOWS_LENGHT
/******************************************************************************
 * Local variables
 *****************************************************************************/
/** Buffer donde se almacena las cuentas que devuelve adIn */
static int32_t muestras[WINDOWS_LENGHT];

/** indice del ultimo valor ingresado en la pila muestras*/
static uint8_t indiceW = 0;

/** Timer de movimiento */
//static uint8_t timer = 0;

/** Promedio de las cuentas */
static int32_t promedio;

/** Promedio de la vuelta anterior en cuentas */
static int32_t promedioAnterior;

/** Promedio Actual */
static uint8_t lecturas_a_promediar = 0;

/******************************************************************************
 * Local Prototypes
 *****************************************************************************/

static void actualizarPromedios(void);
static void almacenaPila(int32_t last_value);
//static void hayMovimiento(void);

/******************************************************************************
 * Local Functions
 *****************************************************************************/

/**
 * @brief Almacena el nuevo valor en la pila
 *
 * @param muestra Cuentas nuevas para agregar a la pila
 */
static void almacenaPila(int32_t last_value)
{
	if( (++indiceW) >= WINDOWS_LENGHT)
	{
		indiceW = 0;
	}
	muestras[indiceW] = last_value;
}

/**
 * FUNCION INTERNA
 *
 * Toma el �ltimo valor de las cuentas y hace el promedio de las
 * �ltimas lecturas_a_promediar muestras. Tambi�n se fija si hubo apertura
 * de filtro y entonces cambia el valor de lecturas_a_promediar
 */
static void actualizarPromedios(void)
{
	uint8_t i;
	uint8_t indice;
	uint32_t auxComp;

	uint32_t ultimaCuenta = muestras[indiceW];

	promedioAnterior = promedio;

	promedio = 0;
	//NOTA: A los fines practicos es lo mismo utilizar siempre
	//el primer span para detectar movimiento.
	/*auxComp = (promedioAnterior > ultimaCuenta)? promedioAnterior-ultimaCuenta : ultimaCuenta - promedioAnterior;
	auxComp*= ajuste->span[0];

	if( auxComp > estabilidad->aperturaFiltro )
	{
		timer = estabilidad->retardoApagado;
		lecturas_a_promediar=0;
		estado.movimiento = SI;
	}*/

	if(lecturas_a_promediar < nivelPromedios)
	{
		lecturas_a_promediar++;
	}

	indice = indiceW;

	for(i=0;i<lecturas_a_promediar;i++)
	{
		if( 0 == indice )
		{
			indice = WINDOWS_LENGHT-1;
		}
		promedio +=  muestras[indice];
		indice--;
	}

	promedio /= lecturas_a_promediar;
}

/**
 * Se fija en los dos �ltimos promedios y en funci�n
 * de eso determina si la lectura est� en movimiento
 * o no. Actualiza el timer de movimiento
 */
/*static void hayMovimiento(void)
{
	float comp;
	int32_t aux;

	aux = (int32_t)(promedio - promedioAnterior);
	comp = absFloat(aux);

	comp = comp * ajuste->span[0] * 10;

	aux = redondeo(comp);

	if( aux >= estabilidad->ventanaMovimiento )
	{
		timer = estabilidad->retardoApagado + 1;
	}

	if( timer )
	{
		aux=1;
		estado.movimiento = SI;
		timer--;
	}
	else
	{
		aux=0;
		estado.movimiento = NO;
	}
}*/


/******************************************************************************
 * Public Functions
 *****************************************************************************/

/**
 * @brief Rutina de filtros
 *
 * @param cuenta Cuentas a filtrar provenientes de AdIn
 *
 * @return promedio Devuelve el promedio ya filtrado
 */
int32_t filtroPrincipal(int32_t adc_in)
{
	almacenaPila(adc_in); // llena buffer con 50 lecturas de cuentas.adIn

	actualizarPromedios(); //devuelve en promedio las cuentas promediadas

	//hayMovimiento();

	return promedio;
}
