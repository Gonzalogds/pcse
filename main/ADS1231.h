/*
 * ADS1231.h
 *
 *  Created on: Feb 9, 2021
 *      Author: Sanchez Gonzalo - na.gonzalo@gmail.com
 */

#ifndef ADS1231_H_
#define ADS1231_H_

/*prototipo para la funci�n callback que el usuario deber� proveer para
 * procesar las lecturas del ADC*/
typedef void (*adc_process_t)(int32_t);

/*
 * Inicializa el puerto SPI y el GPIO correspondiente para comenzar a recibir
 * las interrupciones desde el ADS1231 e inicia la Task que se encarga de la
 * lectura de las conversiones.
 *
 * @param Funcion debe ser un puntero a una funcion que reciba un int32_t como
 * argumento y retorne void. discha funcion sera la encargada de procesar las
 * lecturas.
 * */
void ads1231_init(adc_process_t funcion);

#endif /* ADS1231_H_ */
